#!/bin/bash


# Configurable globals
readonly MIN_IOS_VERSION="10.3"

# This is coming off a fork, arm support is not fully functional from the master repo
# See https://github.com/NativeScript/libffi
readonly LIBFFI_VERSION="bf64a497b4b1da1aae4f14d61761f420ca48cc03"

#readonly GLIB_VERSION="2.47.1"
#readonly GETTEXT_VERSION="0.19.6"
#readonly ICONV_VERSION="1.14"

readonly GLIB_VERSION="2.58.1"
readonly GETTEXT_VERSION="0.20.1"
readonly ICONV_VERSION="1.16"
readonly PCRE_VERSION="8.43"
readonly LIBSOUP_VERSION="2.64.1"
readonly JSON_GLIB_VERSION="1.4.4"
readonly PROTOBUF_C_VERSION="1.3.2"
readonly UNISTRING_VERSION="0.9.10"
readonly LIBIDN_VERSION="1.35"
readonly LIBIDN2_VERSION="2.2.0"
readonly LIBPSL_VERSION="0.21.0"
readonly LIBXML2_VERSION="2.9.9"
readonly SQLITE3_VERSION="3.29.0"

readonly ARCHS=(armv7 arm64)
# armv7s i386 x86_64



# Calculated globals
readonly ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
readonly DEPS_DIR="${ROOT_DIR}/dependencies"
readonly WORK_DIR="${ROOT_DIR}/work"
readonly LOGFILE="${ROOT_DIR}/build.log"

readonly LIPO="$(xcrun --sdk iphoneos -f lipo)"

readonly IPHONEOS_CC="$(xcrun --sdk iphoneos -f clang)"
readonly IPHONEOS_CXX="$(xcrun --sdk iphoneos -f clang++)"
readonly IPHONEOS_SDK=$(xcrun --sdk iphoneos --show-sdk-path)
readonly IPHONEOS_CFLAGS="-isysroot $IPHONEOS_SDK -miphoneos-version-min=$MIN_IOS_VERSION"

readonly IPHONESIM_CC="$(xcrun --sdk iphonesimulator -f clang)"
readonly IPHONESIM_CXX="$(xcrun --sdk iphonesimulator -f clang++)"
readonly IPHONESIM_SDK=$(xcrun --sdk iphonesimulator --show-sdk-path)
readonly IPHONESIM_CFLAGS="-isysroot $IPHONESIM_SDK -mios-simulator-version-min=$MIN_IOS_VERSION"

log() {
  local msg=$1
  local now=$(date "+%Y-%m-%d% %H:%M:%S")

  echo "[${now}] $msg" >> ${LOGFILE}
}

is_file() {
  local file=$1

  [[ -f $file ]]
}

is_dir() {
  local dir=$1

  [[ -d $dir ]]
}

is_empty() {
  local var=$1

  [[ -z $var ]]
}

is_universal() {
  local path=$1

  file $path | grep -q universal
}

is_iphone_arch() {
  local arch=$1

  [[ $arch == arm* ]]
}

fetch() {
  local name=$1
  local url=$2
  local dest=$3

  run "curl -s -L -o $dest $url" "Fetching $name"
}


run() {
  local cmd=$1
  local msg=$2

  log "> ${cmd}"

  echo -n "${msg}..."
  $cmd >> $LOGFILE 2>&1 && echo "done." || {
    log "FAILED with exit code $?"
    echo "failed."
    echo "Build Failed. See ${LOGFILE} for details."
    exit 1
  }
  log "OK."
}

clean_up_prior_build() {
  is_file "${LOGFILE}" \
    && run "rm -f ${LOGFILE}" "Removing old logfile"

  is_dir "${WORK_DIR}" \
    && run "rm -rf ${WORK_DIR}" "Removing old work tree"

  mkdir -p "${WORK_DIR}"
}


# Given an architecture (armv7, i386, etc.) in $1,
# echo out the correct autoconf host triplet
host_for_arch() {
  local readonly arch=$1
  case "$arch" in
    armv*)
      echo "arm-apple-darwin"
      ;;
    arm64)
      echo "aarch64-apple-darwin"
      ;;
    x86_64)
      echo "x86_64-apple-darwin"
      ;;
    i386)
      echo "i386-apple-darwin"
      ;;
    *)
      local msg="ERROR: Unable to determine architecture triplet for $arch"
      log $msg
      echo $msg
      exit 1
  esac
}

# Given an architecture (armv7, i386, etc.) in $1,
# export CFLAGS, CXXFLAGS, etc. appropriate for that arch
set_build_env_for_arch() {
  local readonly arch=$1
  case "$arch" in
    arm*)
      export CC=$IPHONEOS_CC
      export CXX=$IPHONEOS_CXX
      export CFLAGS="$IPHONEOS_CFLAGS"
      ;;
    x86_64 | i386)
      export CC=$IPHONESIM_CC
      export CXX=$IPHONESIM_CXX
      export CFLAGS="$IPHONESIM_CFLAGS"
      ;;
    *)
      local msg="ERROR: Unable to set environment variables for $arch"
      log $msg
      echo $msg
      exit 1
  esac

  export PKG_CONFIG_PATH="${ROOT_DIR}/dependencies/libffi/${arch}/lib/pkgconfig"
  export PKG_CONFIG_PATH="${ROOT_DIR}/dependencies/glib/${arch}/lib/pkgconfig:$PKG_CONFIG_PATH"
  export PKG_CONFIG_PATH="${ROOT_DIR}/dependencies/libidn2/${arch}/lib/pkgconfig:$PKG_CONFIG_PATH"
  export PKG_CONFIG_PATH="${ROOT_DIR}/dependencies/libpsl/${arch}/lib/pkgconfig:$PKG_CONFIG_PATH"
  export PKG_CONFIG_PATH="${ROOT_DIR}/dependencies/libsoup/${arch}/lib/pkgconfig:$PKG_CONFIG_PATH"

  export CFLAGS="$CFLAGS -arch ${arch}"
  export CFLAGS="$CFLAGS -I${ROOT_DIR}/dependencies/gettext/${arch}/include"
  export CFLAGS="$CFLAGS -I${ROOT_DIR}/dependencies/libiconv/${arch}/include"
  export CFLAGS="$CFLAGS -I${ROOT_DIR}/dependencies/pcre/${arch}/include"
  # export CFLAGS="$CFLAGS -I${ROOT_DIR}/dependencies/libintl/${arch}/include"
  export CFLAGS="$CFLAGS -I${ROOT_DIR}/dependencies/glib/${arch}/include"
  export CFLAGS="$CFLAGS -I${ROOT_DIR}/dependencies/protobuf-c/${arch}/include"
  export CFLAGS="$CFLAGS -I${ROOT_DIR}/dependencies/libunistring/${arch}/include"
  export CFLAGS="$CFLAGS -I${ROOT_DIR}/dependencies/libidn2/${arch}/include"
  export CFLAGS="$CFLAGS -I${ROOT_DIR}/dependencies/libpsl/${arch}/include"
  export CFLAGS="$CFLAGS -I${ROOT_DIR}/dependencies/libsoup/${arch}/include"
  export CFLAGS="$CFLAGS -I${IPHONEOS_SDK}/usr/include"

  export CXXFLAGS=$CFLAGS
  export CPPFLAGS=$CFLAGS

  export LDFLAGS="-L${ROOT_DIR}/dependencies/libffi/fat/lib"
  export LDFLAGS="$LDFLAGS -L${ROOT_DIR}/dependencies/gettext/fat/lib"
  export LDFLAGS="$LDFLAGS -L${ROOT_DIR}/dependencies/libiconv/fat/lib"
  export LDFLAGS="$LDFLAGS -L${ROOT_DIR}/dependencies/pcre/fat/lib"
  # export LDFLAGS="$LDFLAGS -L${ROOT_DIR}/dependencies/libintl/fat/lib"
  export LDFLAGS="$LDFLAGS -L${ROOT_DIR}/dependencies/glib/fat/lib"
  export LDFLAGS="$LDFLAGS -L${ROOT_DIR}/dependencies/protobuf-c/fat/lib"
  export LDFLAGS="$LDFLAGS -L${ROOT_DIR}/dependencies/libunistring/fat/lib"
  export LDFLAGS="$LDFLAGS -L${ROOT_DIR}/dependencies/libidn2/fat/lib"
  export LDFLAGS="$LDFLAGS -L${ROOT_DIR}/dependencies/libpsl/fat/lib"
  export LDFLAGS="$LDFLAGS -L${ROOT_DIR}/dependencies/libsoup/fat/lib"
  export LDFLAGS="$LDFLAGS -L${IPHONEOS_SDK}/usr/lib"
  export LDFLAGS="$LDFLAGS -framework Foundation"

  export ac_cv_func__NSGetEnviron=no
  export glib_cv_stack_grows=no
  export glib_cv_uscore=no
  export ac_cv_func_posix_getgrgid_r=yes
  export ac_cv_func_posix_getpwuid_r=yes

  OLD_PATH=$PATH
  # export PATH="${ROOT_DIR}/dependencies/gettext/${arch}/bin:$PATH"
  export PATH="/usr/local/Cellar/gettext/0.20.1/bin:$PATH"
  export GLIB_COMPILE_RESOURCES="/usr/local/Cellar/glib/2.62.0/bin/glib-compile-resources"
  export GLIB_LIBS="-lffi -lgio-2.0 -lglib-2.0 -lgmodule-2.0 -lgobject-2.0 -lgthread-2.0 -lpsl -lpcre -liconv -lintl -lunistring -lz -lidn2 -lresolv"
}

unset_build_env() {
  unset \
    PKG_CONFIG_PATH \
    CC \
    CXX \
    CFLAGS \
    CXXFLAGS \
    CPPFLAGS \
    LDFLAGS \
    \
    ac_cv_func__NSGetEnviron \
    glib_cv_stack_grows \
    glib_cv_uscore \
    ac_cv_func_posix_getgrgid_r \
    ac_cv_func_posix_getpwuid_r

  export PATH=$OLD_PATH
}


# Given a pattern like dependencies/libffi/ARCH/lib/libffi.a
# fatify will locate the static libs for each arch in $ARCHS
# (replacing the magic string 'ARCH' with the appropriate architecture)
# and emit a fat binary in, e.g.,  dependencies/libffi/fat/lib/libffi.a
fatify() {
  local pattern=$1

  local libshortname=$(basename ${pattern})
  local destfile=$(echo ${pattern} | sed s/ARCH/fat/)
  local destdir=$(dirname ${destfile})
  local tempdir="${ROOT_DIR}/.lipo_tmp"

  is_dir "${tempdir}" \
    && run "rm -rf ${tempdir}" "Removing old universal prep temp directory"
  run "mkdir ${tempdir}" "Creating temp directory for universal binary prep"

  local archlist=""
  for arch in "${ARCHS[@]}"; do
    local path=$(echo $pattern | sed s/ARCH/$arch/)
    local libname=${path}

    if $(is_universal $path); then
      local tmpname="${tempdir}/${arch}_${libshortname}"
      local cmd="${LIPO} ${path} -thin ${arch} -output ${tmpname}"
      run "${cmd}" "Extracting $arch from ${path}"
      libname=${tmpname}
    fi

    archlist="${archlist} -arch ${arch} ${libname}"
  done

  run "mkdir -p ${destdir}" "Creating universal binary output directory ${destdir}"

  local cmd="${LIPO} -create -output ${destfile} ${archlist}"
  run "${cmd}" "Creating universal binary for $(basename ${destfile})"
}

build_iconv() {
  local iconvarchive="${WORK_DIR}/iconv.tar.gz"
  local iconvdir="${WORK_DIR}/libiconv-${ICONV_VERSION}"
  local prefix="${DEPS_DIR}/libiconv"

  echo "Beginning build of libiconv"

  ! is_file $iconvarchive \
    && fetch "libiconv" \
      "http://ftp.gnu.org/pub/gnu/libiconv/libiconv-${ICONV_VERSION}.tar.gz" \
      ${iconvarchive}

  for arch in "${ARCHS[@]}"; do
    set_build_env_for_arch ${arch}

    run "env" "Logging environment"

    echo "Building libiconv for $arch"

    is_dir $iconvdir \
      && run "rm -rf $iconvdir" "  Removing old libiconv build directory"

    cd "${WORK_DIR}"
    run "tar xzf ${iconvarchive}" "  Unpacking libiconv"

    cd "${iconvdir}"

    is_iphone_arch ${arch} \
      && sed -i '' 's/if defined __APPLE__ \&\& defined __MACH__/if defined __APPLE__ \&\& defined __MACH__ \&\& defined SKIPIT/' srclib/unistd.in.h

    local host_type=$(host_for_arch ${arch})
    if [ "$arch" == "arm64" ]; then
      host_type="arm-apple-darwin"
    fi

    read -d '' cmd <<EOF
      ./configure \
        --prefix=$prefix/${arch} \
        --enable-static \
        --disable-shared \
        --host=${host_type}
EOF

    run "${cmd}" "  Configuring libiconv for ${arch}"
    run "make -j12" "  Building libiconv for ${arch}"

    run "mkdir -p ${prefix}/${arch}" "  Creating output directory for ${arch}"
    run "make install" "  Installing libiconv for ${arch} into ${prefix}/${arch}"
  done

  cd ${ROOT_DIR}
  local iconvlibs=(libcharset.a libiconv.a)
  for lib in "${iconvlibs[@]}"; do
    fatify "dependencies/libiconv/ARCH/lib/${lib}"
  done

  unset_build_env
  unset cmd
}


build_gettext() {
  local gtarchive="${WORK_DIR}/gt.tar.gz"
  local gtdir="${WORK_DIR}/gettext-${GETTEXT_VERSION}"
  local prefix="${DEPS_DIR}/gettext"

  echo "Beginning build of gettext"

  ! is_file $gtarchive \
    && fetch "gettext" \
      "http://ftp.gnu.org/pub/gnu/gettext/gettext-${GETTEXT_VERSION}.tar.gz" \
      ${gtarchive}

  for arch in "${ARCHS[@]}"; do
    set_build_env_for_arch ${arch}

    run "env" "Logging environment"

    echo "Building gettext for $arch"

    is_dir $gtdir \
      && run "rm -rf $gtdir" "  Removing old gettext build directory"

    cd "${WORK_DIR}"
    run "tar xzf ${gtarchive}" "  Unpacking gettext"

    cd "${gtdir}"

    read -d '' cmd <<EOF
      ./configure \
        --prefix=$prefix/${arch} \
        --enable-static \
        --disable-shared \
        --host=$(host_for_arch ${arch})
EOF

    run "${cmd}" "  Configuring gettext for ${arch}"
    run "make -j12" "  Building gettext for ${arch}"

    run "mkdir -p ${prefix}/${arch}" "  Creating output directory for ${arch}"
    run "make install" "  Installing gettext for ${arch} into ${prefix}/${arch}"
  done

  cd ${ROOT_DIR}
  local gtlibs=(libasprintf.a libgettextpo.a libintl.a)
  for lib in "${gtlibs[@]}"; do
    fatify "dependencies/gettext/ARCH/lib/${lib}"
  done

  unset_build_env
  unset cmd

}

build_glib() {
  local glibzip="${WORK_DIR}/glib.zip"
  local glibdir="${WORK_DIR}/glib-${GLIB_VERSION}"
  local prefix="${DEPS_DIR}/glib"

  echo "Beginning build of glib"

  ! is_file $glibzip \
    && fetch "glib" \
      "https://github.com/GNOME/glib/archive/${GLIB_VERSION}.zip" \
      ${glibzip}

  for arch in "${ARCHS[@]}"; do
    set_build_env_for_arch ${arch}

    run "env" "Logging environment"

    echo "Building glib for $arch"

    is_dir $glibdir \
      && run "rm -rf $glibdir" "  Removing old glib build directory"

    cd "${WORK_DIR}"
    run "unzip ${glibzip}" "  Unpacking glib"

    cd "${glibdir}"
    export NOCONFIGURE=true
    run "./autogen.sh" "  Bootstrapping autoconf for glib"
    unset NOCONFIGURE

    read -d '' cmd <<EOF
      ./configure \
        --prefix=$prefix/${arch} \
        --enable-static \
        --disable-shared \
        --host=$(host_for_arch ${arch}) \
        --with-libiconv=gnu \
        --disable-dtrace
EOF

    run "${cmd}" "  Configuring glib for ${arch}"
    run "make -j12" "  Building glib for ${arch}"

    run "mkdir -p ${prefix}/${arch}" "  Creating output directory for ${arch}"
    run "make install" "  Installing glib for ${arch} into ${prefix}/${arch}"
  done

  cd ${ROOT_DIR}
  local glibs=(libgio-2.0.a libglib-2.0.a libgmodule-2.0.a libgobject-2.0.a libgthread-2.0.a)
  for lib in "${glibs[@]}"; do
    fatify "dependencies/glib/ARCH/lib/${lib}"
  done

  unset_build_env
  unset cmd
}




build_libffi() {
  local ffizip="${WORK_DIR}/ffi.zip"
  local ffidir="${WORK_DIR}/libffi-${LIBFFI_VERSION}"
  local prefix="${DEPS_DIR}/libffi"

  echo "Beginning build of dependency: libffi"

  ! is_file $ffizip \
    && fetch "libffi" \
      "https://github.com/NativeScript/libffi/archive/${LIBFFI_VERSION}.zip" \
      ${ffizip}

  is_dir $ffidir \
    && run "rm -rf $ffidir" "Removing old libffi build directory"

  cd "${WORK_DIR}"
  run "unzip ${ffizip}" "Unpacking libffi"

  cd "${ffidir}"
  run "./autogen.sh" "Bootstrapping autoconf for libffi"

  for arch in "${ARCHS[@]}"; do
    set_build_env_for_arch ${arch}

    echo "Building libffi for $arch"

    is_file config.status \
      && run "make distclean" "  Cleaning up from last run"

    read -d '' cmd <<EOF
      ./configure \
        --prefix=$prefix/${arch} \
        --enable-static \
        --disable-shared \
        --host=$(host_for_arch ${arch})
EOF

    run "${cmd}" "  Configuring libffi for ${arch}"
    run "make -j12" "  Building libffi for ${arch}"

    run "mkdir -p ${prefix}/${arch}" "  Creating output directory for ${arch}"
    run "make install" "  Installing libffi for ${arch} into ${prefix}/${arch}"
  done

  cd ${ROOT_DIR}
  fatify "dependencies/libffi/ARCH/lib/libffi.a"

  unset_build_env
  unset cmd
}




build_libpcre() {
  local ffizip="${WORK_DIR}/pcre.zip"
  local ffidir="${WORK_DIR}/pcre-${PCRE_VERSION}"
  local prefix="${DEPS_DIR}/pcre"

  echo "Beginning build of dependency: pcre"

  ! is_file $ffizip \
    && fetch "pcre" \
      "https://ftp.pcre.org/pub/pcre/pcre-${PCRE_VERSION}.zip" \
      ${ffizip}

  is_dir $ffidir \
    && run "rm -rf $ffidir" "Removing old pcre build directory"

  cd "${WORK_DIR}"
  run "unzip ${ffizip}" "Unpacking pcre"

  cd "${ffidir}"
  # run "./autogen.sh" "Bootstrapping autoconf for pcre"

  for arch in "${ARCHS[@]}"; do
    set_build_env_for_arch ${arch}

    echo "Building libffi for $arch"

    is_file config.status \
      && run "make distclean" "  Cleaning up from last run"

    read -d '' cmd <<EOF
      ./configure \
        --prefix=$prefix/${arch} \
        --enable-static \
        --disable-shared \
        --host=$(host_for_arch ${arch})
EOF

    run "${cmd}" "  Configuring pcre for ${arch}"
    run "make -j12" "  Building pcre for ${arch}"

    run "mkdir -p ${prefix}/${arch}" "  Creating output directory for ${arch}"
    run "make install" "  Installing pcre for ${arch} into ${prefix}/${arch}"
  done

  cd ${ROOT_DIR}
  local gtlibs=(libpcre.a libpcrecpp.a libpcreposix.a)
  for lib in "${gtlibs[@]}"; do
    fatify "dependencies/pcre/ARCH/lib/${lib}"
  done

  unset_build_env
  unset cmd
}


build_libsoup() {
  local ffizip="${WORK_DIR}/libsoup.zip"
  local ffidir="${WORK_DIR}/libsoup-${LIBSOUP_VERSION}"
  local prefix="${DEPS_DIR}/libsoup"

  echo "Beginning build of dependency: libsoup"

  ! is_file $ffizip \
    && fetch "libsoup" \
      "https://gitlab.gnome.org/GNOME/libsoup/-/archive/2.64.1/libsoup-${LIBSOUP_VERSION}.zip" \
      ${ffizip}

  is_dir $ffidir \
    && run "rm -rf $ffidir" "Removing old libsoup build directory"

  cd "${WORK_DIR}"
  run "unzip ${ffizip}" "Unpacking libsoup"

  cd "${ffidir}"
  run "./autogen.sh" "Bootstrapping autoconf for libsoup"

  for arch in "${ARCHS[@]}"; do
    set_build_env_for_arch ${arch}

    echo "Building libsoup for $arch"

    is_file config.status \
      && run "make distclean" "  Cleaning up from last run"

    read -d '' cmd <<EOF
      ./configure \
        --prefix=$prefix/${arch} \
        --enable-static \
        --disable-shared \
        --host=$(host_for_arch ${arch}) \
        --with-gssapi=no \
        --disable-tls-check \
        --without-gnome
EOF

    run "${cmd}" "  Configuring libsoup for ${arch}"
    run "make -j12" "  Building libsoup for ${arch}"

    run "mkdir -p ${prefix}/${arch}" "  Creating output directory for ${arch}"
    run "make install" "  Installing pcre for ${arch} into ${prefix}/${arch}"
  done

  cd ${ROOT_DIR}
  local gtlibs=(libsoup-2.4.a)
  for lib in "${gtlibs[@]}"; do
    fatify "dependencies/libsoup/ARCH/lib/${lib}"
  done

  unset_build_env
  unset cmd
}



build_json_glib() {
  local ffizip="${WORK_DIR}/json-glib.zip"
  local ffidir="${WORK_DIR}/json-glib-${JSON_GLIB_VERSION}"
  local prefix="${DEPS_DIR}/json-glib"

  echo "Beginning build of dependency: json-glib"

  ! is_file $ffizip \
    && fetch "json-glib" \
      "https://gitlab.gnome.org/GNOME/json-glib/-/archive/1.4.4/json-glib-${JSON_GLIB_VERSION}.zip" \
      ${ffizip}

  is_dir $ffidir \
    && run "rm -rf $ffidir" "Removing old pcre build directory"

  cd "${WORK_DIR}"
  run "unzip ${ffizip}" "Unpacking pcre"

  cd "${ffidir}"
  # run "./autogen.sh" "Bootstrapping autoconf for pcre"

  for arch in "${ARCHS[@]}"; do
    set_build_env_for_arch ${arch}

    echo "Building libffi for $arch"

    is_file config.status \
      && run "make distclean" "  Cleaning up from last run"

    rm -rf build
    meson build --cross-file ../../${arch}.txt -Dprefix=/Users/dan/Linux/dependencies/json-glib/${arch} -Ddefault_library=static
    cd build
    ninja
    ninja install
    cd ..

    # read -d '' cmd <<EOF
    #   ./configure \
    #     --prefix=$prefix/${arch} \
    #     --enable-static \
    #     --disable-shared \
    #     --host=$(host_for_arch ${arch})
# EOF

    # run "${cmd}" "  Configuring json-glib for ${arch}"
    # run "make -j12" "  Building json-glib for ${arch}"

    # run "mkdir -p ${prefix}/${arch}" "  Creating output directory for ${arch}"
    # run "make install" "  Installing json-glib for ${arch} into ${prefix}/${arch}"
  done

  cd ${ROOT_DIR}
  local gtlibs=(libjson-glib-1.0.a)
  for lib in "${gtlibs[@]}"; do
    fatify "dependencies/json-glib/ARCH/lib/${lib}"
  done

  unset_build_env
  unset cmd
}




build_protobuf_c() {
  local ffizip="${WORK_DIR}/protobuf-c.zip"
  local ffidir="${WORK_DIR}/protobuf-c-${PROTOBUF_C_VERSION}"
  local prefix="${DEPS_DIR}/protobuf-c"

  echo "Beginning build of dependency: protobuf-c"

  ! is_file $ffizip \
    && fetch "protobuf-c" \
      "https://github.com/protobuf-c/protobuf-c/archive/v${PROTOBUF_C_VERSION}.zip" \
      ${ffizip}

  is_dir $ffidir \
    && run "rm -rf $ffidir" "Removing old protobuf-c build directory"

  cd "${WORK_DIR}"
  run "unzip ${ffizip}" "Unpacking protobuf-c"

  cd "${ffidir}"
  run "./autogen.sh" "Bootstrapping autoconf for protobuf-c"

  for arch in "${ARCHS[@]}"; do
    set_build_env_for_arch ${arch}

    echo "Building protobuf-c for $arch"

    is_file config.status \
      && run "make distclean" "  Cleaning up from last run"

    read -d '' cmd <<EOF
      ./configure \
        --prefix=$prefix/${arch} \
        --enable-static \
        --disable-shared \
        --host=$(host_for_arch ${arch}) \
        --disable-protoc
EOF

    run "${cmd}" "  Configuring protobuf-c for ${arch}"
    run "make -j12" "  Building protobuf-c for ${arch}"

    run "mkdir -p ${prefix}/${arch}" "  Creating output directory for ${arch}"
    run "make install" "  Installing protobuf-c for ${arch} into ${prefix}/${arch}"
  done

  cd ${ROOT_DIR}
  local gtlibs=(libprotobuf-c.a)
  for lib in "${gtlibs[@]}"; do
    fatify "dependencies/protobuf-c/ARCH/lib/${lib}"
  done

  unset_build_env
  unset cmd
}




build_unistring() {
  local ffizip="${WORK_DIR}/unistring.zip"
  local ffidir="${WORK_DIR}/libunistring-${UNISTRING_VERSION}"
  local prefix="${DEPS_DIR}/libunistring"

  echo "Beginning build of dependency: unistring"

  ! is_file $ffizip \
    && fetch "unistring" \
      "https://ftp.gnu.org/gnu/libunistring/libunistring-${UNISTRING_VERSION}.tar.xz" \
      ${ffizip}

  is_dir $ffidir \
    && run "rm -rf $ffidir" "Removing old unistring build directory"

  cd "${WORK_DIR}"
  run "tar xzf ${ffizip}" "  Unpacking unistring"

  cd "${ffidir}"
  # run "./autogen.sh" "Bootstrapping autoconf for unistring"

  for arch in "${ARCHS[@]}"; do
    set_build_env_for_arch ${arch}

    echo "Building protobuf-c for $arch"

    is_file config.status \
      && run "make distclean" "  Cleaning up from last run"

    read -d '' cmd <<EOF
      ./configure \
        --prefix=$prefix/${arch} \
        --host=$(host_for_arch ${arch}) \
        --disable-shared \
        --disable-namespacing
EOF

    run "${cmd}" "  Configuring unistring for ${arch}"
    run "make -j12" "  Building unistring for ${arch}"

    run "mkdir -p ${prefix}/${arch}" "  Creating output directory for ${arch}"
    run "make install" "  Installing unistring for ${arch} into ${prefix}/${arch}"
  done

  cd ${ROOT_DIR}
  local gtlibs=(libunistring.a)
  for lib in "${gtlibs[@]}"; do
    fatify "dependencies/libunistring/ARCH/lib/${lib}"
  done

  unset_build_env
  unset cmd
}


build_libidn() {
  local ffizip="${WORK_DIR}/libidn.zip"
  local ffidir="${WORK_DIR}/libidn-${LIBIDN_VERSION}"
  local prefix="${DEPS_DIR}/libidn"

  echo "Beginning build of dependency: libidn"

  ! is_file $ffizip \
    && fetch "libidn" \
      "https://ftp.gnu.org/gnu/libidn/libidn-${LIBIDN_VERSION}.tar.gz" \
      ${ffizip}

  is_dir $ffidir \
    && run "rm -rf $ffidir" "Removing old libidn build directory"

  cd "${WORK_DIR}"
  run "tar xzf ${ffizip}" "  Unpacking libidn"

  cd "${ffidir}"
  # run "./autogen.sh" "Bootstrapping autoconf for unistring"

  for arch in "${ARCHS[@]}"; do
    set_build_env_for_arch ${arch}

    echo "Building libidn for $arch"

    is_file config.status \
      && run "make distclean" "  Cleaning up from last run"

    read -d '' cmd <<EOF
      ./configure \
        --prefix=$prefix/${arch} \
        --host=$(host_for_arch ${arch}) \
        --disable-shared
EOF

    run "${cmd}" "  Configuring libidn for ${arch}"
    run "make -j12" "  Building libidn for ${arch}"

    run "mkdir -p ${prefix}/${arch}" "  Creating output directory for ${arch}"
    run "make install" "  Installing libidn for ${arch} into ${prefix}/${arch}"
  done

  cd ${ROOT_DIR}
  local gtlibs=(libidn.a)
  for lib in "${gtlibs[@]}"; do
    fatify "dependencies/libidn/ARCH/lib/${lib}"
  done

  unset_build_env
  unset cmd
}


build_libidn2() {
  local ffizip="${WORK_DIR}/libidn2.zip"
  local ffidir="${WORK_DIR}/libidn2-${LIBIDN2_VERSION}"
  local prefix="${DEPS_DIR}/libidn2"

  echo "Beginning build of dependency: libidn2"

  ! is_file $ffizip \
    && fetch "libidn2" \
      "https://ftp.gnu.org/gnu/libidn/libidn2-${LIBIDN2_VERSION}.tar.gz" \
      ${ffizip}

  is_dir $ffidir \
    && run "rm -rf $ffidir" "Removing old libidn2 build directory"

  cd "${WORK_DIR}"
  run "tar xzf ${ffizip}" "  Unpacking libidn2"

  cd "${ffidir}"
  # run "./autogen.sh" "Bootstrapping autoconf for unistring"

  for arch in "${ARCHS[@]}"; do
    set_build_env_for_arch ${arch}

    echo "Building libidn2 for $arch"

    is_file config.status \
      && run "make distclean" "  Cleaning up from last run"

    read -d '' cmd <<EOF
      ./configure \
        --prefix=$prefix/${arch} \
        --host=$(host_for_arch ${arch}) \
        --disable-shared
EOF

    run "${cmd}" "  Configuring libidn2 for ${arch}"
    run "make -j12" "  Building libidn2 for ${arch}"

    run "mkdir -p ${prefix}/${arch}" "  Creating output directory for ${arch}"
    run "make install" "  Installing libidn2 for ${arch} into ${prefix}/${arch}"
  done

  cd ${ROOT_DIR}
  local gtlibs=(libidn2.a)
  for lib in "${gtlibs[@]}"; do
    fatify "dependencies/libidn2/ARCH/lib/${lib}"
  done

  unset_build_env
  unset cmd
}




build_libpsl() {
  local ffizip="${WORK_DIR}/libpsl.zip"
  local ffidir="${WORK_DIR}/libpsl-${LIBPSL_VERSION}"
  local prefix="${DEPS_DIR}/libpsl"

  echo "Beginning build of dependency: libpsl"

  ! is_file $ffizip \
    && fetch "libpsl" \
      "https://github.com/rockdaboot/libpsl/releases/download/libpsl-${LIBPSL_VERSION}/libpsl-${LIBPSL_VERSION}.tar.gz" \
      ${ffizip}

  is_dir $ffidir \
    && run "rm -rf $ffidir" "Removing old libpsl build directory"

  cd "${WORK_DIR}"
  run "tar xzf ${ffizip}" "  Unpacking libpsl"

  cd "${ffidir}"
  # run "./autogen.sh" "Bootstrapping autoconf for unistring"

  for arch in "${ARCHS[@]}"; do
    set_build_env_for_arch ${arch}

    echo "Building libpsl for $arch"

    is_file config.status \
      && run "make distclean" "  Cleaning up from last run"

    read -d '' cmd <<EOF
      ./configure \
        --prefix=$prefix/${arch} \
        --host=$(host_for_arch ${arch}) \
        --disable-shared
EOF

    run "${cmd}" "  Configuring libpsl for ${arch}"
    run "make -j12" "  Building libpsl for ${arch}"

    run "mkdir -p ${prefix}/${arch}" "  Creating output directory for ${arch}"
    run "make install" "  Installing libpsl for ${arch} into ${prefix}/${arch}"
  done

  cd ${ROOT_DIR}
  local gtlibs=(libpsl.a)
  for lib in "${gtlibs[@]}"; do
    fatify "dependencies/libpsl/ARCH/lib/${lib}"
  done

  unset_build_env
  unset cmd
}




build_libxml2() {
  local ffizip="${WORK_DIR}/libxml2.zip"
  local ffidir="${WORK_DIR}/libxml2-${LIBXML2_VERSION}"
  local prefix="${DEPS_DIR}/libxml2"

  echo "Beginning build of dependency: libxml2"

  ! is_file $ffizip \
    && fetch "libxml2" \
      "http://xmlsoft.org/sources/libxml2-${LIBXML2_VERSION}.tar.gz" \
      ${ffizip}

  is_dir $ffidir \
    && run "rm -rf $ffidir" "Removing old libxml2 build directory"

  cd "${WORK_DIR}"
  run "tar xzf ${ffizip}" "  Unpacking libxml2"

  cd "${ffidir}"
  # run "./autogen.sh" "Bootstrapping autoconf for unistring"

  for arch in "${ARCHS[@]}"; do
    set_build_env_for_arch ${arch}

    echo "Building libxml2 for $arch"

    is_file config.status \
      && run "make distclean" "  Cleaning up from last run"

    read -d '' cmd <<EOF
      ./configure \
        --prefix=$prefix/${arch} \
        --host=$(host_for_arch ${arch}) \
        --disable-shared
EOF

    run "${cmd}" "  Configuring libxml2 for ${arch}"
    run "make -j12" "  Building libxml2 for ${arch}"

    run "mkdir -p ${prefix}/${arch}" "  Creating output directory for ${arch}"
    run "make install" "  Installing libxml2 for ${arch} into ${prefix}/${arch}"
  done

  cd ${ROOT_DIR}
  local gtlibs=(libxml2.a)
  for lib in "${gtlibs[@]}"; do
    fatify "dependencies/libxml2/ARCH/lib/${lib}"
  done

  unset_build_env
  unset cmd
}


build_sqlite3() {
  local ffizip="${WORK_DIR}/libsqlite3.zip"
  local ffidir="${WORK_DIR}/sqlite-autoconf-3290000"
  local prefix="${DEPS_DIR}/libsqlite3"

  echo "Beginning build of dependency: libsqlite3"

  ! is_file $ffizip \
    && fetch "libsqlite3" \
      "https://sqlite.org/2019/sqlite-autoconf-3290000.tar.gz" \
      ${ffizip}

  is_dir $ffidir \
    && run "rm -rf $ffidir" "Removing old libsqlite3 build directory"

  cd "${WORK_DIR}"
  run "tar xzf ${ffizip}" "  Unpacking libsqlite3"

  cd "${ffidir}"
  # run "./autogen.sh" "Bootstrapping autoconf for unistring"

  for arch in "${ARCHS[@]}"; do
    set_build_env_for_arch ${arch}

    echo "Building libsqlite3 for $arch"

    is_file config.status \
      && run "make distclean" "  Cleaning up from last run"

    read -d '' cmd <<EOF
      ./configure \
        --prefix=$prefix/${arch} \
        --host=$(host_for_arch ${arch}) \
        --disable-shared
EOF

    run "${cmd}" "  Configuring libsqlite3 for ${arch}"
    run "make -j12" "  Building libsqlite3 for ${arch}"

    run "mkdir -p ${prefix}/${arch}" "  Creating output directory for ${arch}"
    run "make install" "  Installing libsqlite3 for ${arch} into ${prefix}/${arch}"
  done

  cd ${ROOT_DIR}
  local gtlibs=(libsqlite3.a)
  for lib in "${gtlibs[@]}"; do
    fatify "dependencies/libsqlite3/ARCH/lib/${lib}"
  done

  unset_build_env
  unset cmd
}


main() {
  # clean_up_prior_build
  log "Beginning build"

#  build_libffi
 # build_iconv
 # build_gettext
  # build_glib
  # build_libpcre

  # build_libsoup
  build_json_glib
  # build_protobuf_c
    # build_unistring
    # build_libidn
    # build_libidn2
    # build_libpsl
    # build_libxml2
    # build_sqlite3

}

main
