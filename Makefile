MIN = 10.3
ARCHS = armv7 arm64

HDRS = $(wildcard Libraries/$@/*.h)
SRCS = $(wildcard Libraries/$@/*.c)

ROOT = ./ios
PREFIX = $(ROOT)/$@
IDIR = $(PREFIX)/$$ARCH/include
ODIR = $(PREFIX)/$$ARCH/obj
LDIR = $(PREFIX)/$$ARCH/lib

CC = $(shell xcrun --sdk iphoneos -f clang)
LIPO = $(shell xcrun --sdk iphoneos -f lipo)
LIBTOOL = $(shell xcrun --sdk iphoneos -f libtool)
SDK = $(shell xcrun --sdk iphoneos --show-sdk-path)

CFLAGS = -isysroot $(SDK) -miphoneos-version-min=$(MIN) -arch $$ARCH
CFLAGS += -I$(ROOT)
CFLAGS += -I$(ROOT)/glib/$$ARCH/include/glib-2.0
CFLAGS += -I$(ROOT)/glib/$$ARCH/lib/glib-2.0/include
CFLAGS += -I$(ROOT)/protobuf-c/$$ARCH/include
CFLAGS += -I$(ROOT)/json-glib/$$ARCH/include/json-glib-1.0
CFLAGS += -I$(ROOT)/libsoup/$$ARCH/include/libsoup-2.4

LDFLAGS = -L -l

.PHONY: all smarthome-sdk clean

all: glib-ext protobuf-ext json-ext soup-ext smarthome smarthome-sdk

%:
	LIBS=""; \
	mkdir -p $(PREFIX); \
	for HDR in $(HDRS); do \
		cp $$HDR $(PREFIX); \
	done; \
	for ARCH in $(ARCHS); do \
		for SRC in $(SRCS); do \
			OBJ=`echo $$SRC | sed "s/.*\///"`; \
			OBJ="$$OBJ.o"; \
			mkdir -p $(ODIR); \
			$(CC) -c -o $(ODIR)/$$OBJ $$SRC $(CFLAGS); \
		done; \
		mkdir -p $(LDIR); \
		ar cr $(LDIR)/$@.a $(ODIR)/*.o; \
		LIBS="$$LIBS $(LDIR)/$@.a"; \
	done; \
	mkdir -p $(PREFIX)/fat/lib; \
	$(LIPO) -create $$LIBS -output $(PREFIX)/fat/lib/lib$@.a;

smarthome-sdk:
	mkdir -p $(PREFIX)
	cp $(ROOT)/smarthome/smhsdk.h $(PREFIX)/$@.h
	$(LIBTOOL) -static -o $(PREFIX)/lib$@.a $(ROOT)/*/fat/lib/*.a

clean:
	rm -r $(ROOT)/glib-ext
	rm -r $(ROOT)/protobuf-ext
	rm -r $(ROOT)/json-ext
	rm -r $(ROOT)/soup-ext
	rm -r $(ROOT)/smarthome
	rm -r $(ROOT)/smarthome-sdk
